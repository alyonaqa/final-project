import alyonapackage.models.Product;
import alyonapackage.models.User;
import alyonapackage.pages.*;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class www1aCheckoutTest extends CommonTest {
    private User user = new User();

    @Before
    public void setupMe() {
        user.setName("Mary");
        user.setSurname("Poppins");
        user.setEmail("Mary_Poppins@inbox.lv");
        user.setPhone("20202020");
    }

    @Test
    public void checkoutTest() throws InterruptedException {
        getDriver().get("https://www.1a.lv/ru/");

        HeaderPage headerPage = new HeaderPage(getDriver());
        headerPage.clickCookieButton();
        headerPage.inputSearch("Laptop");
        headerPage.clickSearchButton();

        SearchResultPage searchResultPage = new SearchResultPage(getDriver());
        searchResultPage.selectCategoryByName("Ноутбуки");
        searchResultPage.clickFirstCategory();
        searchResultPage.setPrioritySort();
        Thread.sleep(2000);

        Product product = new Product();
        product.setName(searchResultPage.getProductName());
        product.setPrice(searchResultPage.getProductPrice());

        headerPage.handlePromoIFrame();

        searchResultPage.firstItemAddToCart();
        searchResultPage.goToCart();

        CartPage cartPage = new CartPage(getDriver());
        Thread.sleep(2000);
        cartPage.goForward();
        cartPage.goForwardWithoutRegistration();

        OrderPage orderPage = new OrderPage(getDriver());
        orderPage.inputNameField(user.getName());
        orderPage.inputSurnameField(user.getSurname());
        orderPage.inputEmailField(user.getEmail());
        orderPage.inputPhoneField(user.getPhone());
        orderPage.openNotificationsSelect();
        orderPage.notificationDecline();
        orderPage.clickTermsCheckbox();
        orderPage.selectDeliveryInOffice();
        Thread.sleep(2000);
        orderPage.clickForward();

        PaymentMethodPage paymentMethodPage = new PaymentMethodPage(getDriver());
        paymentMethodPage.paymentMethodCashPrivate();
        paymentMethodPage.forwardClickButton();

        SummaryPage summaryPage = new SummaryPage(getDriver());

        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        //complete - page is fully loaded
        assertEquals("complete", js.executeScript("return document.readyState;"));

        String userData = summaryPage.getUserData();

        assertTrue(userData.contains(user.getName()));
        assertTrue(userData.contains(user.getSurname()));
        assertTrue(userData.contains(user.getEmail()));
        assertTrue(userData.contains(user.getPhone()));

        assertEquals(product.getName(), summaryPage.getProductName());
        assertEquals(product.getPrice(), summaryPage.getProductPrice(), 0.0);
    }


}
