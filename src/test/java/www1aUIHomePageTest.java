import alyonapackage.pages.FooterPage;
import alyonapackage.pages.HeaderPage;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class www1aUIHomePageTest extends CommonTest {

    @Test
    public void headerFooterCheckTest() {
        getDriver().get("https://www.1a.lv/ru/");

        HeaderPage headerPage = new HeaderPage(getDriver());
        headerPage.clickCookieButton();
        headerPage.handlePromoIFrame();

        assertEquals("info@1a.lv", headerPage.getHeaderEmail());
        assertEquals("67428800", headerPage.getHeaderPhone());
        assertEquals("О нас", headerPage.getHeaderPar1aLv());

        FooterPage footerPage = new FooterPage(getDriver());
        assertEquals("Контакты", footerPage.getContacts());
        assertEquals("Доставка товаров", footerPage.getDelivery());
        assertEquals("Лизинг",footerPage.getLeasing());
        assertEquals("Гарантия и возврат", footerPage.getWarrantyReturn());
        assertEquals("Как купить?", footerPage.getWhereToBuy());
        assertEquals("О нас", footerPage.getAboutUs());
        assertEquals("Для СМИ", footerPage.getForSmi());
        assertEquals("Свяжись с нами", footerPage.getContactInfo());
        assertEquals("(+371) 67428800", footerPage.getPhoneFirst());
        assertEquals("(+371) 67428801", footerPage.getPhoneSecond());
        assertEquals("info@1a.lv", footerPage.getMail());
        assertTrue(footerPage.getAddress().contains("Мукусалас 41, Рига"));
        assertEquals("Как нас найти", footerPage.getHowToFind());

        headerPage.dropdownOpen();
        headerPage.goToContactsPage();
        headerPage.dropdownOpen();
        headerPage.goToDeliveryProductsPage();
        headerPage.dropdownOpen();
        headerPage.goToLeasingPage();
        headerPage.dropdownOpen();
        headerPage.goToDisclaimerPage();
        headerPage.dropdownOpen();
        headerPage.goToHowToBuyPage();
        headerPage.dropdownOpen();
        headerPage.goToPurchaseAgreementPage();
        headerPage.dropdownOpen();
        headerPage.goToPrivacyPolicyPage();
        headerPage.dropdownOpen();
        headerPage.goToMediaPage();
        headerPage.dropdownOpen();
        headerPage.goToAboutPage();
    }

}
