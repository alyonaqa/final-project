package alyonapackage.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SummaryPage {

    private WebDriver driver;

    private By userData = By.cssSelector("div.cart-info div.col-1");
    private By productPrice = By.id("shopping-cart-total-amount");
    private By productName = By.cssSelector("div.ait-cart-item-info");

    public SummaryPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getUserData() {
        return driver.findElement(userData).getText();
    }

    public String getProductName() {
        return driver.findElement(productName).getText();
    }

    public Double getProductPrice() {
        String price = driver.findElement(productPrice).getText();
        price = price.substring(0, price.length()-2);

        return Double.valueOf(price);
    }
}
