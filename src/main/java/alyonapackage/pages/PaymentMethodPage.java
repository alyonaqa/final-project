package alyonapackage.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PaymentMethodPage {

    private WebDriver driver;

    private By paymentCashMethod = By.xpath("//div[@class='select-method']//a[@payment_type='CASH_INDIVIDUAL_PERSON']");
    private By forwardButton = By.cssSelector("div.ait-checkout-footer a.order-form-forward-button");

    public PaymentMethodPage(WebDriver driver) {
        this.driver = driver;
    }

    public void paymentMethodCashPrivate() {
        driver.findElement(paymentCashMethod).click();
    }

    public void forwardClickButton() {
        driver.findElement(forwardButton).click();
    }
}
