package alyonapackage.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartPage {

    private WebDriver driver;

    private By forwardButton = By.cssSelector("div.ait-checkout-header a.order-form-forward-button");
    private By forwardButtonWithoutRegistration = By.cssSelector("div.not-reg a.btn-v2");

    public CartPage(WebDriver driver) {
        this.driver = driver;
    }

    public void goForward() {
        driver.findElement(forwardButton).click();
    }

    public void goForwardWithoutRegistration() {
        driver.findElement(forwardButtonWithoutRegistration).click();
    }
}
