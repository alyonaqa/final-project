package alyonapackage.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class CommonPage {

    private WebDriver driver;

    private String promoFrameIdPart = "mt-";

    private By cookieOkButton = By.id("CybotCookiebotDialogBodyLevelButtonAccept");
    private By footerSliderCloseButton = By.cssSelector("div.close-button-slider");

    public CommonPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return this.driver;
    }

    public void clickCookieButton() {
        driver.findElement(cookieOkButton).click();
    }

    public void handlePromoIFrame() {
        WebElement frame = getPromoFrame();
        if (frame != null) {
            driver.switchTo().frame(frame);
            driver.findElement(footerSliderCloseButton).click();
        }
    }

    private WebElement getPromoFrame() {
        WebElement promoFrame = null;

        List<WebElement> frames = driver.findElements(By.tagName("iframe"));

        for (int i = 0; i < frames.size(); i++) {
            if (frames.get(i).getAttribute("id").contains(promoFrameIdPart)) {
                promoFrame = frames.get(i);
                break;
            }
        }

        return promoFrame;
    }
}
