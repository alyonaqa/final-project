package alyonapackage.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class OrderPage {

    private WebDriver driver;

    private By nameInput = By.id("order_main_data_name");
    private By surnameInput = By.id("order_main_data_surname");
    private By emailInput = By.id("order_main_data_email");
    private By phoneInput = By.id("order_main_data_contact_phone_number");
    private By notificationsMethodSelect = By.cssSelector("div.select-method a.ui-button-icon");
    private By notificationsMethodOptionToEmail = By.cssSelector("#ui-id-2 li:nth-child(1)");
    private By notificationsMethodOptionDecline = By.cssSelector("#ui-id-2 li:nth-child(2)");
    private By termsCheckbox = By.id("accept_purchase_agreement");
    private By deliveryTypeInOffice = By.cssSelector("div#available_delivery_types_container article:last-child");
    private By orderForwardButton = By.cssSelector("div.ait-checkout-footer a.order-form-forward-button");

    public OrderPage(WebDriver driver) {
        this.driver = driver;
    }

    public void inputNameField(String name) {
        driver.findElement(nameInput).sendKeys(name);
    }

    public void inputSurnameField(String surname) {
        driver.findElement(surnameInput).sendKeys(surname);
    }

    public void inputEmailField(String email) {
        driver.findElement(emailInput).sendKeys(email);
    }

    public void inputPhoneField(String phone) {
        driver.findElement(phoneInput).sendKeys(phone);
    }

    public void openNotificationsSelect() {
        driver.findElement(notificationsMethodSelect).click();
    }

    public void notificationToEmail() {
        driver.findElement(notificationsMethodOptionToEmail).click();
    }

    public void notificationDecline() {
        driver.findElement(notificationsMethodOptionDecline).click();
    }

    public void clickTermsCheckbox() {
        driver.findElement(termsCheckbox).click();
    }

    public void selectDeliveryInOffice() {
        driver.findElement(deliveryTypeInOffice).click();
    }

    public void clickForward() {
        driver.findElement(orderForwardButton).click();
    }
}
