package alyonapackage.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HeaderPage extends CommonPage {

    private By headerEmail = By.cssSelector("ul.toplinks a.mail");
    private By headerPhone = By.cssSelector("ul.toplinks a.tel");
    private By headerPar1aLv = By.cssSelector("ul.toplinks a.by");
    private By par1aContacts = By.cssSelector("ul.toplinks div.dropdown a[href='/ru/contacts'");
    private By par1aDeliveryProducts = By.cssSelector("ul.toplinks div.dropdown a[href='/ru/product_delivery_and_guarantees");
    private By par1aLeasing = By.cssSelector("ul.toplinks div.dropdown a[href='/ru/leasing'");
    private By par1aDisclaimer = By.cssSelector("ul.toplinks div.dropdown a[href='/ru/disclaimer'");
    private By par1aHowToBuy = By.cssSelector("ul.toplinks div.dropdown a[href='/ru/how_to_buy'");
    private By par1aPurchaseAgreement = By.cssSelector("ul.toplinks div.dropdown a[href='/ru/purchase_agreement'");
    private By par1aPrivacyPolicy = By.cssSelector("ul.toplinks div.dropdown a[href='/ru/about#privacy-policy'");
    private By par1aMedia = By.cssSelector("ul.toplinks div.dropdown a[href='/ru/media'");
    private By par1aAbout = By.cssSelector("ul.toplinks div.dropdown a[href='/ru/about'");
    private By searchProductField = By.id("search_products_text_field");
    private By searchButton = By.cssSelector("form#autocomplete_form input:last-child");

    public HeaderPage(WebDriver driver) {
        super(driver);
    }

    public String getHeaderEmail() {
        return getDriver().findElement(headerEmail).getText();
    }

    public String getHeaderPhone() {
        return getDriver().findElement(headerPhone).getText();
    }

    public String getHeaderPar1aLv() {
        return getDriver().findElement(headerPar1aLv).getText();
    }

    public void inputSearch(String searchString) {
        getDriver().findElement(searchProductField).sendKeys(searchString);
    }

    public void clickSearchButton() {
        getDriver().findElement(searchButton).click();
    }

    public void dropdownOpen() {
        getDriver().findElement(headerPar1aLv).click();
    }

    public void goToContactsPage() {
        getDriver().findElement(par1aContacts).click();
    }

    public void goToDeliveryProductsPage() {
        getDriver().findElement(par1aDeliveryProducts).click();
    }

    public void goToLeasingPage() {
        getDriver().findElement(par1aLeasing).click();
    }

    public void goToDisclaimerPage() {
        getDriver().findElement(par1aDisclaimer).click();
    }

    public void goToHowToBuyPage() {
        getDriver().findElement(par1aHowToBuy).click();
    }

    public void goToPurchaseAgreementPage() {
        getDriver().findElement(par1aPurchaseAgreement).click();
    }

    public void goToPrivacyPolicyPage() {
        getDriver().findElement(par1aPrivacyPolicy).click();
    }

    public void goToMediaPage() {
        getDriver().findElement(par1aMedia).click();
    }

    public void goToAboutPage() {
        getDriver().findElement(par1aAbout).click();
    }
}
