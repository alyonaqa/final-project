package alyonapackage.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchResultPage {

    private WebDriver driver;

    private By prioritySort = By.cssSelector("div.select-sort-products");
    private By priorityOptionRatingCount = By.cssSelector("div.drop-product-sort-list li:last-child");
    private By firstItemAddToCartButton = By.cssSelector("section.product a.btn-cart");
    private By goToCartButton = By.id("shopping_cart_container");
    private By firstProductName = By.cssSelector("section.product h3");
    private By firstProductPrice = By.cssSelector("section.product div.price");
    private By firstCategory = By.cssSelector("div.ait-search-results-images a:first-child");

    public SearchResultPage(WebDriver driver) {
        this.driver = driver;
    }

    public void selectCategoryByName(String categoryName) {
        driver.findElement(By.linkText(categoryName)).click();
    }

    public void clickFirstCategory() {
        driver.findElement(firstCategory).click();
    }

    public void setPrioritySort() {
        driver.findElement(prioritySort).click();
        driver.findElement(priorityOptionRatingCount).click();
    }

    public void firstItemAddToCart() {
        driver.findElement(firstItemAddToCartButton).click();
    }

    public void goToCart() {
        driver.findElement(goToCartButton).click();
    }

    public String getProductName() {
        return driver.findElement(firstProductName).getText();
    }

    public Double getProductPrice() {
        String price = driver.findElement(firstProductPrice).getText();
        String cents = price.substring(price.length() - 2);
        price = price.substring(1, price.length() - 2) + "." + cents;

        return Double.valueOf(price);
    }
}
