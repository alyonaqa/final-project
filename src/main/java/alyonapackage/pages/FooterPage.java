package alyonapackage.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FooterPage {

    private WebDriver driver;

    private By contactsFooter = By.cssSelector("ul.footer-links a[href='/ru/contacts'");
    private By deliveryFooter = By.cssSelector("ul.footer-links a[href='/ru/product_delivery_and_guarantees'");
    private By leasingFooter = By.cssSelector("ul.footer-links a[href='/ru/leasing'");
    private By warrantyReturnFooter = By.cssSelector("ul.footer-links a[href='/ru/disclaimer'");
    private By howToBuyFooter = By.cssSelector("ul.footer-links a[href='/ru/how_to_buy'");
    private By aboutUsFooter = By.cssSelector("ul.footer-links a[href='/ru/about'");
    private By forSmiFooter = By.cssSelector("ul.footer-links a[href='/ru/media'");
    private By contactInfoFooter = By.cssSelector("div.contact-info strong.title");
    private By phoneNumberFirstFooter = By.cssSelector("div.contact-info ul.tel a[href='tel:+37167428800'");
    private By phoneNumberSecondFooter = By.cssSelector("div.contact-info ul.tel a[href='tel:+37167428801'");
    private By mailFooter = By.cssSelector("div.contact-info a[href='mailto:info@1a.lv'");
    private By addressFooter = By.cssSelector("div.contact-info address.address");
    private By howToFindFooter = By.cssSelector("div.contact-info a[href='http://www.1a.lv/ru/contacts'");


    public FooterPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getContacts() {
        return driver.findElement(contactsFooter).getText();
    }

    public String getDelivery() {
        return driver.findElement(deliveryFooter).getText();
    }

    public String getLeasing() {
        return driver.findElement(leasingFooter).getText();
    }

    public String getWarrantyReturn() {
        return driver.findElement(warrantyReturnFooter).getText();
    }

    public String getWhereToBuy() {
        return driver.findElement(howToBuyFooter).getText();
    }

    public String getAboutUs() {
        return driver.findElement(aboutUsFooter).getText();
    }

    public String getForSmi() {
        return driver.findElement(forSmiFooter).getText();
    }

    public String getContactInfo() {
        return driver.findElement(contactInfoFooter).getText();
    }

    public String getPhoneFirst() {
        return driver.findElement(phoneNumberFirstFooter).getText();
    }

    public String getPhoneSecond() {
        return driver.findElement(phoneNumberSecondFooter).getText();
    }

    public String getMail() {
        return driver.findElement(mailFooter).getText();
    }

    public String getAddress() {
        return driver.findElement(addressFooter).getText();
    }

    public String getHowToFind() {
        return driver.findElement(howToFindFooter).getText();
    }


}
