# Alyona K Automation Project

Basic test automation project for learning

## Getting Started

Use git clone to get a copy on your local machine

### Prerequisites

- git
- java 8
- IJ IDEA community
- lombok plugin
- annotation preprocessor enabled

### Installing

- git clone this project
- enable annotation preprocessor

## Running the tests

* test are located in src/test
* run them using IDEA, no additional parameters required.
* chrome driver binary located in test/resources

## Built With

* [Gradle] (https://gradle.org/)

## Authors

* **Alyona K** - *Initial work*
